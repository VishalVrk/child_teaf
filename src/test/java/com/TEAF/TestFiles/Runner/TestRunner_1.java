package com.TEAF.TestFiles.Runner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.TEAF.framework.GenerateCustomReport;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.JsonReader;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.TestConfig;
import com.TEAF.framework.Utilities;
import com.TEAF.stepDefinitions.GalenStep;
import com.galenframework.config.GalenConfig;
import com.galenframework.config.GalenProperty;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@CucumberOptions(
		// dryRun = false,
		plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
				"pretty",
				//"html:CukeNativeHTMLReport/",
				//"junit:src/test/java/com/TestResults/cucumber-report/cucumber_1.xml",
		"json:src/test/java/com/TestResults/cucumber-report/cucumber_1.json"},
		strict = true, 
		junit="--step-notifications",
		features = { "src/test/java/com/TEAF/TestFiles/Features" }, 
		glue = { "com.TEAF.Hooks","com.TEAF.stepDefinitions"}, 
		//tags = { "@PartnerSettings","~@Ignore"},
		//tags = { "@AddTimeslot,@EditTimeslot,@DeleteTimeslot","~@Ignore"},
		//tags = { "@EditTimeslot","~@Ignore"},
		tags = {"@Sanity","not @Ignore"},
		monochrome = false
		)

@RunWith(Cucumber.class)
//@RunWith(ExtendedCucumberRunner.class)
public class TestRunner_1{

	static String Platform = null;
	static String Browser = null;
	static String reportPath=null;

	//static String AppUrl = "https://stg8.augustasportswear.com/";

	@BeforeClass
	public static void setUp() {
		try {
			//LoadConfigurations
			TestConfig.LoadAllConfig();
			Platform = System.getProperty("test.platformName","desktop");
			Browser = System.getProperty("test.browserName","chrome");
			
			//Report configurations
			DateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm_ss");
			Date date = new Date();
			String timeStamp=dateFormat.format(date);
			reportPath = "output/Report-"+timeStamp;
			
			if(System.getProperty("test.DisableExtentReport").equalsIgnoreCase("false")) {
				System.setProperty("screenshot.dir", reportPath+"/Screenshots/");
				System.setProperty("extent.reporter.html.start","true");
				System.setProperty("extent.reporter.spark.start","true");
				System.setProperty("extent.reporter.html.out", reportPath+"/ExtentTestReport.html");
				System.setProperty("extent.reporter.spark.out", reportPath+"/");
			

				//System.setProperty("extent.reporter.logger.start", "true");
				//System.setProperty("extent.reporter.html.config","src/test/java/com/Resources/extent-config.xml");
				//System.setProperty("extent.reporter.logger.out", "output/LoggerOutput/");
				//System.setProperty("extent.reporter.klov.start", "true");
				//System.setProperty("extent.reporter.klov.config", "klov.properties");
			}
			
			//Galen Property
			GalenConfig.getConfig().setProperty(GalenProperty.SCREENSHOT_FULLPAGE, "true");

			//ScreenVideoCapture
			if(System.getProperty("test.DisableScreenVideoCapture").equalsIgnoreCase("false"))
			{
				Utilities.startVideoRecorder();
			}
			//SessionSetup
			if(Platform.equals("desktop")||Platform.equals("mobile")){
				StepBase.setUp(Platform,Browser);	
			}
			else if(Platform.equals("android") ||Platform.equals("ios") ) {
				//Specify Device configuration Json file path here 
				System.setProperty("test.appPackage",TestConfig.objConfig.getProperty("test.appPackage"));
				System.setProperty("test.appActivity",TestConfig.objConfig.getProperty("test.appActivity"));
				JsonReader.ReadJson(System.getProperty("user.dir") + "/src/test/java/com/Cucumber/mobileConfig/android/nodeConfigOnePlus.json");
				if(TestConfig.objConfig.get("test.AppType").equals("webapp")){
					//StepBase.appiumStart(HashMapContainer.get("port"), objConfig.getProperty("nodePath"), objConfig.getProperty("appiumJSPath"), null);
				}else{
					StepBase.appiumStart(HashMapContainer.get("port"), TestConfig.objConfig.getProperty("nodePath"), TestConfig.objConfig.getProperty("appiumJSPath"), TestConfig.objConfig.getProperty("test.appName"));
				}
				//Disable following line if connecting device via cable
				//HashMapContainer.add("udid", "10.22.175.97:5555");
				//HashMapContainer.add("udid", "192.168.0.101:5555");
				//*********************
				//Thread.sleep(10000);
				StepBase.setUp(Platform,Browser);	
			}else{
				System.out.println("Enter valid platform choice: desktop / android / ios");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void tearDown() {
		try {
			//WrapperFunctions.sendMail("C:\\Users\\user\\workspace\\CucumberFramework\\output\\Run_1475150446542\\report.html", "swathin@royalcyber.com");
			GalenStep.generateUIReport();
			StepBase.tearDown();

			//Generation of Default Cucumber Reports
			if(!System.getProperty("test.DisableCucumberReport").equalsIgnoreCase("true"))
			{
				//Cucumber Report Generation
				GenerateCustomReport.generateCustomeReport(Browser, Platform);
				HashMapContainer.ClearHM();
			}

			//Prepare reports for Email
			if (System.getProperty("test.generateEmail").equalsIgnoreCase("false")) {
				Utilities.reportstoZipFile(reportPath, "TestExecution_ExtentReports");
				Utilities.reportstoZipFile("UITestReports", "TestExecution_UIReports");
				Utilities.auto_generation_Email("swathin@royalcyber.com","premkumar.g@royalcyber.com");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
