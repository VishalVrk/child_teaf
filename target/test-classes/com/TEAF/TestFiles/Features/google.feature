@google 
Feature: Google search test script! 

@Sanity 
Scenario Outline: Do google search for given keywords and verify correct search resutl is returned.
	Given My WebApp 'Google' is open 
	And I should see element 'googlesearCH2' present on page_
    When I enter '<SearchKeyword>' in field '<field>' 
	#And I click 'seaRCHBtn' 
	#Then I should see element '<searchResult>' present on page
	#And I should see text '<ExpectedText>' present on page at '<searchResult>'
	
	Examples: 
		|SearchKeyword  |field		  |ExpectedText								  |	searchResult 	  |
		|cucumber	   	|googlesearch2|test          							  |CucumberResultImage|
		|appium			|googlesearch |Appium: Mobile App Automation Made Awesome.|AppiumResult		  |